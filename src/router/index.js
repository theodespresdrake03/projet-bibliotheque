import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Onebook from '../views/Onebook.vue'
import categories from "@/views/categories";
import Onecategorie from "@/views/Onecategorie";
import mentionslegales from "@/views/mentionslegales";
import livresindispo from "@/views/livresindispo";
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/books/:id',
    name: 'books',
    component: Onebook
  },
  {
    path: '/categories',
    name: 'categories',
    component: categories
  },
  {
    path: '/categorie/:id',
    name: 'categorie',
    component: Onecategorie
  },
  {
    path: '/mentionslegales',
    name: 'mentionslegales',
    component: mentionslegales
  },
  {
    path: '/livresindispo',
    name: 'livresindispo',
    component: livresindispo
  },

  {
    path: '/ajout',
    name: 'ajout',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
